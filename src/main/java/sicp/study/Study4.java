package sicp.study;

import java.util.function.UnaryOperator;

interface Term {
    Integer func(Integer x);
}

interface Supplier {
    Integer next(Integer x);
}

public class Study4 {
    public static void main(String[] args) {
        System.out.println(sum(new Term() {
            public Integer func(Integer x) {
                return x * x;
            }
        }, 1, new Supplier() {
            public Integer next(Integer x) {
                return x + 1;
            }
        }, 4));

        System.out.println(sum2(x -> x * x, 1, x -> x + 1, 4));
        System.out.println(sum3(x -> x * x, 1, x -> x + 1, 4));
    }

    public static Integer sum(Term term, Integer a, Supplier supplier, Integer b) {
        if (a > b) {
            return 0;
        } else {
            return term.func(a) + sum(term, supplier.next(a), supplier, b);
        }
    }

    public static Integer sum2(UnaryOperator<Integer> term, Integer a, UnaryOperator<Integer> next, Integer b) {
        if (a > b) {
            return 0;
        } else {
            return term.apply(a) + sum2(term, next.apply(a), next, b);
        }
    }

    /**
     * 연습문제 1.30
     * @param term
     * @param a
     * @param next
     * @param b
     * @return
     */
    public static Integer sum3(UnaryOperator<Integer> term, Integer a, UnaryOperator<Integer> next, Integer b) {
        Integer result = 0;
        for (Integer i = a; i <= b; i = next.apply(i)) {
            result += term.apply(i);
        }
        return result;
    }
}

